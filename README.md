## Skrypt użytkownika do zmniejszenia czcionki i odstępów na stronie z listą produktów w backendzie Magento 2 ##

Szersze informacje: 
https://21w.pl/skrypt-uzytkownika-do-zmniejszenia-czcionki-i-odstepow-na-stronie-z-lista-produktow-w-backendzie-magento-2/

Mile widziane propozycje usprawnień.

*English:*
## Userscript to decrease size of fonts and paddings on table of products in Magento 2 backend ##

Feel free to share your opinions or ideas.